/**
     This is the FINAL version of this example

     - c_str() is not used
     - To compile it make sure you use C++11 

     In my case this is done with the line
        g++ -std=c++11 helloRevisited.cpp
 */

#include <fstream>

using std::ofstream;
using std::string; 

int main(){

   string nameOfFile = "output1.txt";
   ofstream fout;
   fout.open( nameOfFile );

   fout << "Hello, Land of the Forgotten!\n";

   fout.close();
   return 0;
}
